# Webcam filter

A command-line webcam filter for shy people.
This is an unfinished project.

![](mask.png)

Starting from a blog post by
[Adrian Rosebrock](https://www.pyimagesearch.com/2017/04/24/eye-blink-detection-opencv-python-dlib/)
I wanted to create a sort of instagram filter in Python.
The idea is to use the face detector to then attach any
sort of mask to faces in the frame.

_Current status_: a solid color is painted on the detected
faces, drawing a sort of mask.
A blinking and FPS counters are also displayed.

### Usage

Default usage
(it will download the necessary face detector model
for the DLib library and save it in the `data` directory):
```
python filter.py
```
This will open a window and show a live feed in full
resolution of the detected faces on top of the
original video.
To quit the program from the window, press the `q` key.

To get help:
```
python filter.py -h
```

### Dependencies

Use a conda virtual environment.
Set it up with
```
$ conda env install -f environment.yml
```

### Shape predictor for DLib

<https://github.com/davisking/dlib-models/raw/master/shape_predictor_68_face_landmarks.dat.bz2>