import argparse
import bz2
import time
import urllib.request
from pathlib import Path
from typing import Union

import cv2
import dlib
import imutils
import numpy as np

# from imutils.video import FileVideoStream
# from imutils.video import VideoStream
from imutils import face_utils
from scipy.spatial import distance as dist

# define two constants, one for the eye aspect ratio to indicate
# blink and then a second constant for the number of consecutive
# frames the eye must be below the threshold
FACE_PREDICTOR_URL = "https://github.com/davisking/dlib-models/raw/master/shape_predictor_68_face_landmarks.dat.bz2"
FACE_PREDICTOR_PATH = Path("data/shape_predictor_68_face_landmarks.dat")
EYE_AR_THRESHOLD = 0.2
EYE_AR_CONSECUTIVE_FRAMES = 3
# grab the indexes of the facial landmarks
(LEFT_EYE_START, LEFT_EYE_END) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(RIGHT_EYE_START, RIGHT_EYE_END) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
(MOUTH_START, MOUTH_END) = face_utils.FACIAL_LANDMARKS_IDXS["mouth"]
(NOSE_START, NOSE_END) = face_utils.FACIAL_LANDMARKS_IDXS["nose"]
(INNER_MOUTH_START, INNER_MOUTH_END) = face_utils.FACIAL_LANDMARKS_IDXS["inner_mouth"]
(LEFT_EYEBROW_START, LEFT_EYEBROW_END) = face_utils.FACIAL_LANDMARKS_IDXS[
    "left_eyebrow"
]
(RIGHT_EYEBROW_START, RIGHT_EYEBROW_END) = face_utils.FACIAL_LANDMARKS_IDXS[
    "right_eyebrow"
]
(JAW_START, JAW_END) = face_utils.FACIAL_LANDMARKS_IDXS["jaw"]


def eye_aspect_ratio(eye):
    """Compute the avg eye aspect ratio (for blinking)"""
    # compute the euclidean distances between the two sets of
    # vertical eye landmarks (x, y)-coordinates
    height_a = dist.euclidean(eye[1], eye[5])
    height_b = dist.euclidean(eye[2], eye[4])
    # compute the euclidean distance between the horizontal
    # eye landmark (x, y)-coordinates
    width = dist.euclidean(eye[0], eye[3])
    # compute the eye aspect ratio
    aspect_ratio = (height_a + height_b) / (2.0 * width)
    # return the eye aspect ratio
    return aspect_ratio


def draw_shape_dots(frame, shape):
    """Loop over the (x, y)-coordinates and draw them on the image"""
    for (x, y) in shape:
        cv2.circle(frame, (x, y), 1, (0, 0, 255), -1)


def detect_increment_blinks(left_eye, right_eye, blink_frames_counter, n_blinks_total, frame):
    """Calculate blinks and print info"""
    # Ayes aspect ratios for blink detectors
    left_eye_ar = eye_aspect_ratio(left_eye)
    right_eye_ar = eye_aspect_ratio(right_eye)
    # average the eye aspect ratio together for both eyes
    avg_eye_ar = (left_eye_ar + right_eye_ar) / 2.0
    # check to see if the eye aspect ratio is below the blink
    # threshold, and if so, increment the blink frame counter
    if avg_eye_ar < EYE_AR_THRESHOLD:
        blink_frames_counter += 1
    # otherwise, the eye aspect ratio is not below the blink
    # threshold
    else:
        # if the eyes were closed for a sufficient number of frames
        # then increment the total number of blinks
        if blink_frames_counter >= EYE_AR_CONSECUTIVE_FRAMES:
            n_blinks_total += 1
        # reset the eye frame counter
        blink_frames_counter = 0
    # draw the total number of blinks on the frame along with
    # the computed eye aspect ratio for the frame
    cv2.putText(
        frame,
        "Blinks: {}".format(n_blinks_total),
        (10, 30),
        cv2.FONT_HERSHEY_SIMPLEX,
        0.7,
        (0, 0, 255),
        2,
    )
    cv2.putText(
        frame,
        "EAR: {:.2f}".format(avg_eye_ar),
        (300, 30),
        cv2.FONT_HERSHEY_SIMPLEX,
        0.7,
        (0, 0, 255),
        2,
    )
    return blink_frames_counter, n_blinks_total


def draw_mask(
    face_shape, frame, facial_features,
):
    # Add forehead segment
    apex_angle = np.degrees(
        -np.arctan2(
            -(facial_features["nose"][0, 0] - facial_features["nose"][6, 0]),
            -(facial_features["nose"][0, 1] - facial_features["nose"][6, 1]),
        )
    )
    apex_angle = int(apex_angle)
    ellipse_center = (  # facial_features["nose"][0, 0], facial_features["nose"][0, 1])
        facial_features["nose"][0, 0]
        + (facial_features["nose"][0, 0] - facial_features["nose"][6, 0]) // 2,
        facial_features["nose"][0, 1]
        + (facial_features["nose"][0, 1] - facial_features["nose"][6, 1]) // 2,
    )
    cv2.circle(frame, ellipse_center, 1, (0, 255, 255), -1)
    ellipse = cv2.ellipse2Poly(
        center=ellipse_center,
        axes=(100, 50),
        angle=180 + apex_angle,
        arcStart=40,
        arcEnd=140,
        delta=10,
    )
    # cv2.drawContours(frame, [left_eye_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [right_eye_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [left_eyebrow_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [right_eyebrow_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [inner_mouth_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [mouth_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [facial_features["nose"]_hull], -1, (0, 255, 0), 1)
    # cv2.drawContours(frame, [shape_hull], -1, (0, 255, 0), 1)
    # compute the convex hull for the left and right eye, then
    # visualize each of the eyes
    left_eye_hull = cv2.convexHull(facial_features["left_eye"])
    right_eye_hull = cv2.convexHull(facial_features["right_eye"])
    left_eyebrow_hull = cv2.convexHull(facial_features["left_eyebrow"])
    right_eyebrow_hull = cv2.convexHull(facial_features["right_eyebrow"])
    mouth_hull = cv2.convexHull(facial_features["mouth"])
    nose_hull = cv2.convexHull(facial_features["nose"])
    inner_mouth_hull = cv2.convexHull(facial_features["inner_mouth"])
    # Connect face points with forehead ellipse
    shape_hull = cv2.convexHull(np.vstack((face_shape, ellipse)))
    # # Fill polygons to create mask
    cv2.fillConvexPoly(frame, shape_hull, (255, 0, 0))
    cv2.fillConvexPoly(frame, left_eye_hull, (0, 0, 0))
    cv2.fillConvexPoly(frame, right_eye_hull, (0, 0, 0))
    cv2.fillConvexPoly(frame, left_eyebrow_hull, (0, 0, 0))
    cv2.fillConvexPoly(frame, right_eyebrow_hull, (0, 0, 0))
    cv2.fillConvexPoly(frame, mouth_hull, (0, 0, 255))
    cv2.fillConvexPoly(frame, nose_hull, (0, 0, 255))
    cv2.fillConvexPoly(frame, inner_mouth_hull, (0, 0, 0))
    # Highlight some special points
    cv2.circle(
        frame,
        (facial_features["nose"][0, 0], facial_features["nose"][0, 1]),
        1,
        (0, 255, 255),
        -1,
    )
    cv2.circle(
        frame,
        (facial_features["nose"][6, 0], facial_features["nose"][6, 1]),
        1,
        (0, 255, 255),
        -1,
    )
    cv2.circle(
        frame,
        (facial_features["jaw"][0, 0], facial_features["jaw"][0, 1]),
        1,
        (0, 255, 255),
        -1,
    )
    cv2.circle(
        frame,
        (facial_features["jaw"][-1, 0], facial_features["jaw"][-1, 1]),
        1,
        (0, 255, 255),
        -1,
    )
    # cv2.fillConvexPoly(frame, ellipse, (0, 255, 0))


def predict_face(gray, predictor, rect, scaling_ratio):
    """
    Determine the facial landmarks for the face region, then
    convert the facial landmark (x, y)-coordinates to a NumPy array
    """
    scaled_shape = predictor(gray, rect)
    face_shape = (face_utils.shape_to_np(scaled_shape) * scaling_ratio).astype(np.int64)
    return face_shape


def draw_new_face(face_shape, frame):
    draw_shape_dots(frame, face_shape)
    # extract the facial features coordinates
    facial_features = dict(
        left_eye=face_shape[LEFT_EYE_START:LEFT_EYE_END],
        right_eye=face_shape[RIGHT_EYE_START:RIGHT_EYE_END],
        left_eyebrow=face_shape[LEFT_EYEBROW_START:LEFT_EYEBROW_END],
        right_eyebrow=face_shape[RIGHT_EYEBROW_START:RIGHT_EYEBROW_END],
        mouth=face_shape[MOUTH_START:MOUTH_END],
        nose=face_shape[NOSE_START:NOSE_END],
        inner_mouth=face_shape[INNER_MOUTH_START:INNER_MOUTH_END],
        jaw=face_shape[JAW_START:JAW_END],
    )
    draw_mask(
        face_shape, frame, facial_features,
    )
    return facial_features


def download_shape_predictor(
    file_path: Union[str, Path] = FACE_PREDICTOR_PATH, url: str = FACE_PREDICTOR_URL,
):
    if not isinstance(file_path, Path):
        file_path = Path(file_path)
    if file_path.exists():
        print(f"Shape predictor found at {file_path.as_posix()}")
    else:
        print(f"{file_path.as_posix()} not found.\n" f"Downloading now from {url}...")
        if not file_path.parent.exists():
            file_path.parent.mkdir()
            print(f"Created missing folder '{file_path.parent.as_posix()}/'.")
        bz2_bytes = urllib.request.urlopen(url).read()
        with open(FACE_PREDICTOR_PATH, "wb") as out_file_handler:
            out_file_handler.write(bz2.decompress(bz2_bytes))


def main():
    # initialize the frame counters and the total number of blinks
    blink_frames_counter = 0
    n_blinks_total = 0
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    # We use a 68-point face predictor from dlib
    ap.add_argument(
        "-p",
        "--shape-predictor",
        default=FACE_PREDICTOR_PATH.as_posix(),
        # required=True,
        help="path to facial landmark predictor",
        type=str,
    )
    ap.add_argument(
        "-v", "--video", type=str, default="", help="path to input video file"
    )
    args = vars(ap.parse_args())
    # Download and unpack face detection model if necessary
    download_shape_predictor(file_path=args["shape_predictor"])
    # initialize dlib's face detector (HOG-based) and then create
    # the facial landmark predictor
    print("[INFO] loading facial landmark predictor...")
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(args["shape_predictor"])
    # # start the video stream thread
    # print("[INFO] starting video stream thread...")
    # # vs = FileVideoStream(args["video"]).start()
    # file_stream = True
    # video_stream = VideoStream(src=0).start()
    # file_stream = False
    time.sleep(0.2)
    capture = cv2.VideoCapture(0)
    print(f"Capture FPS: {capture.get(cv2.CAP_PROP_FPS)}")
    # Set up codec for HD stream (high framerate allowed)
    width = 1280
    height = 720
    fps = 24
    capture.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M", "J", "P", "G"))
    capture.set(cv2.CAP_PROP_FPS, fps)
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    # resized_height = 450
    scaling_ratio = 2  # height / resized_height
    print(f" == Video properties == ")
    print(f"Resolution: \t" f"{width}x" f"{height}")
    print(f"FPS:      \t\t{fps}")
    # rectangles_detected = []
    n_iter = 0
    detection_interval = 2
    position_average_interval = 4
    face_shapes = []
    avg_face_shapes = []
    while True:
        # Capture frame-by-frame
        valid, frame = capture.read()
        if not valid:
            break

        if (n_iter % detection_interval) == 0:
            # Our operations on the frame come here
            resized_frame = imutils.resize(frame, height=height // scaling_ratio)
            gray_frame = cv2.cvtColor(resized_frame, cv2.COLOR_BGR2GRAY)
            # detect faces in the grayscale frame
            rectangles = detector(gray_frame, 0)
            if rectangles:
                rectangles_detected = rectangles
                face_shapes.append(
                    np.array(
                        [
                            predict_face(gray_frame, predictor, rect, scaling_ratio)
                            for rect in rectangles_detected
                        ]
                    )
                )
                # face_shapes = [
                #     predict_face(gray_frame, predictor, rect, scaling_ratio)
                #     for rect in rectangles_detected
                # ]
        else:
            pass

        # if len(face_shapes) > 1:
        #     print(face_shapes)
        #     exit()
        if face_shapes:
            avg_face_shapes = face_shapes[-1]
            if (n_iter % position_average_interval) == 0:
                # TODO: Find a way to average between predictions (we are only taking the last for now)
                shapes = np.array(face_shapes)
                print(f"shapes.shape: {shapes.shape}")
                print(shapes)
                # The axes here are: iterations, shapes in the frame, x, y
                # print(np.sum(shapes[:, 0, :, :], axis=0))
                # exit()
                n_faces = shapes.shape[1]
                avg_face_shapes = [
                    (np.sum(shapes[:, i, :, :], axis=0) / shapes.shape[0]).astype(
                        np.int64
                    )
                    for i in range(n_faces)
                ]
                # avg_face_shapes = face_shapes[-1]
                face_shapes = []

        # loop over the face detections
        # for rect in rectangles_detected:
        #     face_shape = predict_face(gray_frame, predictor, rect, scaling_ratio)
        for face_shape in avg_face_shapes:

            facial_features = draw_new_face(face_shape, frame)

            blink_frames_counter, n_blinks_total = detect_increment_blinks(
                facial_features["left_eye"],
                facial_features["right_eye"],
                blink_frames_counter,
                n_blinks_total,
                frame,
            )

        # Print FPS
        cv2.putText(
            frame,
            f"{capture.get(cv2.CAP_PROP_FPS):3.2f} FPS",
            (150, 30),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.7,
            (0, 0, 255),
            2,
        )
        # Show some helpful text
        cv2.putText(
            frame,
            "Press 'q' to quit",
            (10, 60),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.7,
            (0, 0, 255),
            2,
        )

            # show the frame
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break
        n_iter += 1
    # do a bit of cleanup
    capture.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
